#include "Graph.h"
#include "GraphSearch.h"
#include <iostream>

int main(int argc, char *argv[]) {
   Graph inst(std::cin);
   inst.saveDot("grafo.dot");

   DepthFirstSearch dfs(inst);
   std::cout << "DFS Solve = " << dfs.solve() << "\n";

   
   return 0;
}
