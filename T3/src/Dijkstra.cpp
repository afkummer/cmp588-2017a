#include "Dijkstra.h"
#include <iostream>
#include <limits>

Dijkstra::Dijkstra(const Graph &inst): inst(inst) {
   qu.reserve(inst.getVertexCount());
}

Dijkstra::~Dijkstra() {
   // Empty dtor.
}

void Dijkstra::solve(unsigned int start) {
   const decltype(Heap::Node::value) inf =std::numeric_limits<decltype(Heap::Node::value)>::max();
   visited.resize(inst.getVertexCount(), 0),
   dist.resize(inst.getVertexCount(), inf);

   dist[start] = 0;
   qu.insert(start, 0);

   while (!qu.isEmpty()) {
      Heap::Node node = qu.deleteMin();
      visited[node] = 1;
      for (const Graph::Neighbor &neigh: inst.getSucc(node.key)) {
         if (visited[neigh] == 0) {
            const unsigned newDist = dist[node] + neigh.capacity;
            if (dist[neigh] == inf) {
               dist[neigh] = newDist;
               qu.insert(neigh.vertex, dist[neigh]);
            } else if (newDist < dist[neigh]) {
               dist[neigh] = newDist;
               qu.decreaseKey(neigh.vertex, newDist);
            }
         }
      }
   }
}

unsigned int Dijkstra::getDistance(unsigned int dest) const {
   return dist[dest];
}

std::ostream &operator<<(std::ostream &out, const Dijkstra &dij) {
   // TODO make me real!
   return out;
}