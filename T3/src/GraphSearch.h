#pragma once

#include "Graph.h"
#include <queue>
#include <vector>
#include <iosfwd>
#include <limits>

/**
 * Implementação da busca em profundidade em grafo.
 */
class DepthFirstSearch {
public:
   struct Frame {
      unsigned vertex;
      unsigned minFlow;
      unsigned rminFlow;
      unsigned idx{0}, maxIdx;
      unsigned ridx{0}, maxRidx;
   };

   DepthFirstSearch(Graph &g);
   ~DepthFirstSearch();

   bool solve();

private:
   Graph &graph;
   std::vector<unsigned char> visited;
   std::vector<Frame> queue;
   std::vector<Frame>::iterator top;

   void refreshTopFlow(bool direct, bool residual);
   void setTopVertex(unsigned vertex);
};

