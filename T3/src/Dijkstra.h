#pragma once

#include "Graph.h"
#include "Heap.h"
#include <vector>

class Dijkstra {
public:
   Dijkstra(const Graph &inst);
   ~Dijkstra();

   void solve(unsigned start);
   unsigned getDistance(unsigned dest) const;

   friend std::ostream &operator<<(std::ostream &out, const Dijkstra &dij);
private:
   const Graph &inst;
   std::vector <unsigned char> visited;
   std::vector <unsigned int>  dist;
   Heap qu;
};
