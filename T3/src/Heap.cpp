#include "Heap.h"
#include <iostream>
#include <cstdlib>

Heap::Node::operator unsigned int() const {
   return key;
}

Heap::Node::operator std::size_t() const {
   return key;
}

bool Heap::Node::operator<(const Heap::Node &other) const {
   return value < other.value;
}

bool Heap::Node::operator>(const Heap::Node &other) const {
   return value > other.value;
}

Heap::Heap() {
   char *ch = std::getenv("HEAP_ARITY");
   if (ch == nullptr) {
      arity = 2;
   } else {
      arity = std::stoi(ch);
   }
}

Heap::~Heap() {
   // Vazio.
}

bool Heap::isEmpty() const {
   return array.empty();
}

void Heap::insert(unsigned int key, unsigned int value) {
   array.push_back({key, value});
   mapping[key] = array.size()-1;
   siftUp(array.size()-1);
}

Heap::Node Heap::deleteMin() {
   Heap::Node root = array.front();
   array.front() = array.back();
   array.pop_back();
   mapping[array.front()] = 0;
   siftDown(0);
   return root;
}

void Heap::decreaseKey(unsigned int key, unsigned int newValue) {
   const std::size_t pos = mapping[key];
   Heap::Node &n = array[pos];
   n.value = newValue;
   siftUp(pos);
}

std::ostream& operator<<(std::ostream &out, const Heap &h) {
   out << "digraph Heap_" << h.arity << " {" << std::endl;
   bool done = false;
   for (unsigned i = 0; !done; ++i) {
      for (unsigned j = 0; j < h.arity; ++j) {
         const unsigned pos = h.child(i, j);
         if (pos >= h.array.size()) {
            done = true;
            break;
         }
         out << '\t' << h.array[i].key << " -> " << h.array[pos].key << ';' << std::endl;
      }
   }
   out << "}";
   return out;
}

unsigned Heap::getArity() const {
   return arity;
}

unsigned Heap::child(unsigned parent, unsigned th) const {
   return parent *arity + th + 1;
}

unsigned Heap::parent(unsigned child) const {
   return (child-1) / arity;
}

void Heap::siftUp(unsigned pos) {
   while (pos != 0) {
      const unsigned p = parent(pos);
      if (array[p] > array[pos]) {
         std::swap(array[p], array[pos]);
         std::swap(mapping[array[p]], mapping[array[pos]]);
         pos = p;
      } else {
         break;
      }
   }
}

void Heap::siftDown(unsigned pos) {
   while (true) {
      unsigned next = child(pos, 0);
      for (unsigned i = 1; i < arity; ++i) {
         unsigned ch = child(pos, i);
         if (ch >= array.size())
            break;
         if (array[next] > array[ch]) {
            next = ch;
         }
      }
      if (next < array.size() && array[next] < array[pos]) {
         std::swap(array[next], array[pos]);
         std::swap(mapping[array[next]], mapping[array[pos]]);
         pos = next;
      } else {
         break;
      }
   }
}

void Heap::reserve(std::size_t cap) {
   array.reserve(cap);
   mapping.reserve(cap);
}

const std::vector <Heap::Node> &Heap::getArray() const {
   return array;
}
