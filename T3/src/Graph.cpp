#include "Graph.h"
#include <iostream>
#include <sstream>
#include <fstream>

Graph::Neighbor::operator std::size_t() const {
   return vertex;
}

Graph::Graph(std::istream &in) {
   char line[100];
   unsigned idGen = 0;
   while (true) {
      in.getline(line, 100);
      if (!in)
         break;
      if (line[0] == 'p') {
         std::sscanf(line, "%*c %*s %u %u", &vertexCount, &edgeCount);
         succ.resize(vertexCount);
         pred.resize(vertexCount);
      } else if (line[0] == 'n') {
         char id;
         decltype(s) vertex;
         std::sscanf(line, "%*c %u %c", &vertex, &id);
         if (id == 's')
            s = vertex-1;
         else
            t = vertex-1;
      } else if (line[0] == 'a') {
         Neighbor neigh;
         unsigned origin;
         std::sscanf(line, "%*c %u %u %u", &origin, &neigh.vertex, &neigh.capacity);
         origin -= 1;
         neigh.vertex -= 1;
         neigh.id = idGen++;
         succ[origin].push_back(neigh);
         std::swap(origin, neigh.vertex);
         pred[origin].push_back(neigh);
      }
   }
}

const std::vector <Graph::Neighbor> &Graph::getSucc(unsigned vertex) const {
   return succ[vertex];
}

const std::vector< Graph::Neighbor > &Graph::getPred(unsigned int vertex) const {
   return pred[vertex];
}

std::ostream &operator<<(std::ostream &out, const Graph &inst) {
   out << "p\tmax\t" << inst.getVertexCount() << '\t' << inst.getEdgeCount() << '\n';
   out << "n\t" << inst.s+1 << "\ts\n";
   out << "n\t" << inst.t+1 << "\tt\n";
   for (unsigned i = 0; i < inst.getVertexCount(); ++i) {
      for (const Graph::Neighbor &j: inst.getPred(i)) {
         out << "a\t" << j.vertex+1 << '\t' << i+1 << '\t' << j.capacity << '\n';
      }
   }
   return out;
}

unsigned int Graph::getEdgeCount() const {
   return edgeCount;
}

unsigned int Graph::getVertexCount() const {
   return vertexCount;
}

unsigned int Graph::getS() const {
   return s;
}

unsigned int Graph::getT() const {
   return t;
}

void Graph::saveDot(const char fname[]) const {
   std::ofstream fid(fname);
   fid << "digraph Grafo {\n";
   for (unsigned i = 0; i < vertexCount; ++i) {
      for (const Neighbor &j: pred[i])
         fid << '\t' << i << " -> " << j.vertex << ";\n";
   }
   fid << "}\n";
   fid.close();
}


