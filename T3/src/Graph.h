#pragma once

#include <iosfwd>
#include <vector>

class Graph {
public:

   struct Neighbor {
      unsigned id;
      unsigned vertex;
      unsigned capacity;
      unsigned flow{0};
      operator std::size_t() const;
   };

   Graph(std::istream &in);

   const std::vector<Neighbor> &getSucc(unsigned vertex) const;
   const std::vector<Neighbor> &getPred(unsigned vertex) const;

   unsigned getVertexCount() const;
   unsigned getEdgeCount() const;

   unsigned getS() const;
   unsigned getT() const;

   void saveDot(const char fname[]) const;

   friend std::ostream &operator<<(std::ostream &out, const Graph &i);

private:
   unsigned vertexCount, edgeCount;
   unsigned s, t;
   std::vector <std::vector<Neighbor>> succ, pred;
};
