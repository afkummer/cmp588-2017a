#pragma once

#include <vector>
#include <iosfwd>

class Heap {
public:

   struct Node {
      unsigned int key;
      unsigned int value;

      operator std::size_t() const;
      operator unsigned int() const;
      bool operator>(const Node &other) const;
      bool operator<(const Node &other) const;
   };

   /** Obedece a variável de ambiente `HEAP_ARITY` para definição
    * da aridade do heap.
    */
   Heap();
   ~Heap();

   bool isEmpty() const;
   unsigned getArity() const;
   std::size_t getSwapCount() const;

   Node deleteMin();
   void insert(unsigned int key, unsigned int value);
   void decreaseKey(unsigned int key, unsigned int newValue);

   void reserve(std::size_t cap);
   const std::vector <Node> &getArray() const;

   friend std::ostream &operator<<(std::ostream &out, const Heap &h);

private:
   std::vector <Node> array;
   std::vector <unsigned int> mapping;
   unsigned arity;

   unsigned child(unsigned int parent, unsigned int th) const;
   unsigned parent(unsigned int child) const;
   void siftUp(unsigned int pos);
   void siftDown(unsigned int pos);
};
