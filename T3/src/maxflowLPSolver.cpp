#include "Graph.h"
#include <iostream>

#include <glpk.h>

int main() {
   // Instance loading.
   Graph inst(std::cin);

   // Setup GLPK environment.
   glp_prob *prob = glp_create_prob();
   glp_set_prob_name(prob, "maxflow");
   glp_set_obj_dir(prob, GLP_MAX);

   // Set tableau layout.
   glp_add_rows(prob, inst.getVertexCount());
   glp_add_cols(prob, inst.getEdgeCount());

   // Cache of frequently used information.
   char buf[100];
   unsigned s = inst.getS(), t = inst.getT();

   // Set constraint names and bounds.
   for (unsigned int i = 0; i < inst.getVertexCount(); ++i) {
      std::sprintf(buf, "flow_%u", i+1);
      glp_set_row_name(prob, i+1, buf);
      glp_set_row_bnds(prob, i+1, GLP_FX, 0.0, 0.0);
   }

   // Set flow conservation constraints and arc names.
   for (unsigned i = 0; i < inst.getVertexCount(); ++i) {
      // Prepare the memory structure to set flow constraints.
      const unsigned int len = inst.getSucc(i).size() + inst.getPred(i).size();
      unsigned pos = 1;
      std::vector <int>     ind(len+1);
      std::vector <double>  val(len+1);

      // Set "output" arcs of vertex `i'.
      for (const Graph::Neighbor &j: inst.getSucc(i)) {
         ind[pos] = j.id+1;
         val[pos] = -1.0;
         ++pos;

         // Set varibale name, bounds, ant type.
         glp_set_col_bnds(prob, j.id+1, GLP_DB, 0.0, j.capacity);
         std::sprintf(buf, "arc_%u_%u", i+1, j.vertex+1);
         glp_set_col_name(prob, j.id+1, buf);
         glp_set_col_kind(prob, j.id+1, GLP_IV);
      }

      // Set "input" arcs of vertex `i'.
      for (const auto &j: inst.getPred(i)) {
         ind[pos] = j.id+1;
         val[pos] = 1.0;
         ++pos;
      }

      // Populate the constraint.
      glp_set_mat_row(prob, i+1, pos-1, ind.data(), val.data());
   }

   // Set objective linear coeficients (flow capacity of each arc).
   for (const auto &j: inst.getSucc(s)) {
      glp_set_obj_coef(prob, j.id+1, j.capacity);
   }

   // Change the sense of flow constraints of s and t.
   glp_set_row_bnds(prob, s+1, GLP_UP, 0.0, 0.0);
   glp_set_row_bnds(prob, t+1, GLP_LO, 0.0, 0.0);

   // Solve the linear program with SIMPLEX method.
   glp_smcp params;
   glp_init_smcp(&params);
   params.msg_lev = GLP_MSG_OFF;
   glp_simplex(prob, &params);

   // Compute total flow comming from `s'.
   double maxFlow = 0.0;
   for (const Graph::Neighbor &i: inst.getSucc(s)) {
      maxFlow += glp_get_col_prim(prob, i.id+1);
   }

   // Show max flow solution.
   std::cout << maxFlow;

   // Release memory used by GLPK model.
   glp_delete_prob(prob);
   glp_free_env();

   return 0;
}
