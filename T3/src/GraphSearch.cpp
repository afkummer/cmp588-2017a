#include "GraphSearch.h"

DepthFirstSearch::DepthFirstSearch(Graph& g):
   graph(g),
   visited(g.getVertexCount()),
   queue(g.getVertexCount()+1) {

   // Adiciona um nodo dummy no início da pilha.
   queue.front().rminFlow = 0;
   queue.front().minFlow = 0;
}

DepthFirstSearch::~DepthFirstSearch() {
   // Vazio.
}

bool DepthFirstSearch::solve() {
   // Etapa da inicialização da estrutura de memória usada pelo
   // algoritmo de busca em profundidade não-recursivo.
   visited.assign(visited.size(), 0);
   top = queue.begin()+1;

   // Configura o nodo de saída do grafo (source).
   setTopVertex(graph.getS());
   refreshTopFlow(true, true);
   visited[graph.getS()] = 1;

   while(top != queue.begin()) {
      if (top->vertex == graph.getT())
         break;

      bool flag = true;
      // Explora o grafo na ordem dos sucessores.
      if (top->idx < top->maxIdx) {
         const Graph::Neighbor &n = graph.getSucc(top->vertex)[top->idx];
         unsigned diff = n.capacity - n.flow;
         if (visited[n.vertex] == 0 && diff > 0) {
            top->idx += 1;
            ++top;
            setTopVertex(n.vertex);
            top->minFlow = std::min((top-1)->minFlow, diff);
         }
         visited[n.vertex] = 1;
         flag = false;
      }

      // Explora o grafo na ordem dos predecessores, se houver fluxo
      // nas arestas  em questão (grafo residual).
      if (top->ridx < top->maxRidx) {
         const Graph::Neighbor &n = graph.getPred(top->vertex)[top->ridx];
         if (visited[n.vertex] == 0 && n.flow > 0) {
            top->ridx += 1;
            ++top;
            setTopVertex(n.vertex);
            top->rminFlow = std::min((top-1)->rminFlow, n.flow);
         }
         visited[n.vertex] = 1;
         flag = false;
      }

      // Sem progresso a partir do vértice corrente.
      // Faz um "pop".
      if (flag) {
         --top;
      }
   }


   return top != queue.begin() && top->vertex == graph.getT();
}

void DepthFirstSearch::refreshTopFlow(bool direct, bool residual) {
   if (direct && top->idx < top->maxIdx) {
      const Graph::Neighbor &n0 = graph.getSucc(top->vertex)[top->idx];
      unsigned diff = n0.capacity - n0.flow;
      top->minFlow = std::min((top-1)->minFlow, diff);
   }
   if (residual && top->ridx < top->maxRidx) {
      const Graph::Neighbor &n0 = graph.getPred(top->vertex)[top->ridx];
      top->rminFlow = std::min((top-1)->rminFlow, n0.flow);
   }
}

void DepthFirstSearch::setTopVertex(unsigned int vertex) {
   top->vertex = vertex;
   top->maxIdx = graph.getSucc(vertex).size();
   top->maxRidx = graph.getPred(vertex).size();
   top->idx = 0;
   top->ridx = 0;
}




