#include "Util.h"
#include "Dijkstra.h"
#include <iostream>
#include <fstream>
#include <cstring>

int main(int argc, char *argv[]) {
   if (argc != 3) {
      std::cout << -1 << std::endl;
      return 1;
   }
   const unsigned origin = std::stoul(argv[1])-1;
   const unsigned dest = std::stoul(argv[2])-1;
   Instance inst(std::cin);
   Dijkstra <Heap> dij(inst);
   dij.solve(origin);
   const unsigned dist = dij.distance(dest);
   std::cout << dist << std::endl;
   return 0;
}
