#include "Heap.h"
#include "Util.h"
#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <chrono>
#include <unistd.h>

const std::size_t REPEAT = 10;

int main() {
   std::cout << "h;max-value;wtime-secs;steady-time-secs;swap-count;memusage-kbytes" << std::endl;
   for (unsigned h = 1; h < 30; ++h) {
      const unsigned long ub = std::pow(2, h);
      double timeAcc = 0.0;
      double timeAccC = 0.0;
      std::size_t sumSwapCount = 0;
      for (std::size_t rep = 0; rep < REPEAT; ++rep) {
         Heap heap;
         heap.reserve(ub);
         double time = walltime();
         auto tc0 = std::chrono::steady_clock::now();
         for (unsigned long i = ub-1; i > 0; --i) {
            heap.insert(i, i);
         }
         auto tc1 = std::chrono::steady_clock::now();
         timeAcc += (walltime()-time);
         auto elap = std::chrono::duration<double>(tc1-tc0);
         timeAccC += elap.count();
         sumSwapCount += heap.swapCount();
      }
      std::cout <<
         h << ';' <<
         ub-1 << ';' <<
         timeAcc/static_cast<double>(REPEAT) << ';' <<
         timeAccC/static_cast<double>(REPEAT) << ';' <<
         static_cast<std::size_t>(sumSwapCount/static_cast<double>(REPEAT)) << ';' <<
         memusage() << std::endl;
   }
   return 0;
}
