#pragma once

#include "PQueue.h"
#include <vector>

class LinearPQueue {
public:
   LinearPQueue();
   ~LinearPQueue();
   PQueue::Node deletemin();
   void insert(PQueue::Node::KeyType vertex, PQueue::Node::ValueType weight);
   void decreasekey(PQueue::Node::KeyType vertex, PQueue::Node::ValueType newWeight);
   bool empty() const;
   friend std::ostream& operator<<(std::ostream &out, const LinearPQueue &heap);
private:
   std::vector <PQueue::Node> m_nodes;
};
