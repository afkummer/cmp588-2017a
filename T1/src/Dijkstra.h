#pragma once

#include "Instance.h"
#include "LinearPQueue.h"
#include "Heap.h"

#include <iostream>
#include <vector>
#include <limits>

template <typename Q>
class Dijkstra {
public:
   Dijkstra(const Instance &inst): m_inst(inst) {
      // Vazio
   }
   ~Dijkstra() {
      // Vazio
   }
   void solve(unsigned start) {
      const PQueue::Node::ValueType inf = std::numeric_limits<PQueue::Node::ValueType>::max();
      m_visited.resize(m_inst.vertexCount(), 0),
      m_dist.resize(m_inst.vertexCount(), inf);
      m_dist[start] = 0;
      m_qu.insert(start, 0);
      while (!m_qu.empty()) {
         PQueue::Node node = m_qu.deletemin();
         m_visited[node] = 1;
         for (const Instance::Neighbor &neigh: m_inst.neighbors(node.getKey())) {
            if (m_visited[neigh] == 0) {
               const unsigned newDist = m_dist[node] + neigh.m_dist;
               if (m_dist[neigh] == inf) {
                  m_dist[neigh] = newDist;
                  m_qu.insert(neigh.m_vertex, m_dist[neigh]);
               } else if (newDist < m_dist[neigh]) {
                  m_dist[neigh] = newDist;
                  m_qu.decreasekey(neigh.m_vertex, newDist);
               }
            }
         }
      }
   }
   unsigned distance(unsigned dest) const {
      return m_dist[dest];
   }
   friend std::ostream &operator<<(std::ostream &out, const Dijkstra &dij) {
      std::printf("%8s   %8s   %7s\n", "index", "dist", "visited");
      for (std::size_t i = 0; i < dij.m_visited.size(); ++i) {
         std::printf("%8zu   %8u   %7u\n", i, dij.m_dist[i], static_cast<unsigned>(dij.m_visited[i]));
      }
      return out;
   }
private:
   const Instance &m_inst;
   Q m_qu;
   std::vector <unsigned char> m_visited;
   std::vector <PQueue::Node::ValueType>  m_dist;
};
