#include "Instance.h"
#include "Util.h"
#include <iostream>
#include <sstream>

Instance::Instance(std::istream &in) {
   std::string line;

   while (line.substr(0,4) != "p sp")
      std::getline(in, line);

   std::stringstream buf(line);
   buf.ignore(5);
   buf >> m_vertexCount >> m_edgeCount;

   m_graph.resize(m_vertexCount);

   std::size_t curEdge = 0;
   while (curEdge < m_edgeCount) {
      std::getline(in, line);
      if (line[0] != 'a')
         continue;

      buf.clear();
      buf.str(line);
      buf.ignore(2, ' ');

      unsigned origin, dest, dist;
      buf >> origin >> dest >> dist;

      m_graph[origin-1].push_back({dest-1, dist});
      ++curEdge;
   }

   // Faz um ajuste fino de uso da memória.
   for (auto &i: m_graph)
      i.shrink_to_fit();
}

const std::vector <Instance::Neighbor> &Instance::neighbors(unsigned vertex) const {
   return m_graph[vertex];
}

std::ostream &operator<<(std::ostream &out, const Instance &inst) {
   out << "digraph Graph {" << std::endl;
   for (unsigned i = 0; i < inst.m_vertexCount; ++i) {
      for (const Instance::Neighbor &j: inst.neighbors(i)) {
         out << '\t' << i << " -> " << j.m_vertex << ';' << std::endl;
      }
   }
   out << '}' << std::endl;
   return out;
}
