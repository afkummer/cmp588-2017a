#pragma once

#include <iosfwd>
#include <vector>
#include <unordered_map>

class Instance {
public:

   struct Neighbor {
      unsigned m_vertex;
      unsigned m_dist;

      inline operator std::size_t() const {
         return m_vertex;
      }
   };

   Instance(std::istream &in);

   const std::vector<Neighbor> &neighbors(unsigned vertex) const;

   inline unsigned vertexCount() const {
      return m_vertexCount;
   }

   inline unsigned edgeCount() const {
      return m_edgeCount;
   }

   friend std::ostream &operator<<(std::ostream &out, const Instance &i);


private:
   unsigned m_vertexCount, m_edgeCount;
   std::vector<std::vector<Neighbor>> m_graph;
};