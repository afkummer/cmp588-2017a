#include "PQueue.h"
#include <limits>

PQueue::Node::Node() {
   // Vazio
}

PQueue::Node::Node(Node::KeyType key, Node::ValueType weight):
   m_key(key),
   m_weight(weight) {
   // Vazio
}

PQueue::Node::operator std::size_t() const {
   return getKey();
}

bool PQueue::Node::operator==(const Node::KeyType key) {
   return getKey() == key;
}


PQueue::Node::KeyType PQueue::Node::getKey() const {
   return m_key;
}


PQueue::Node::ValueType PQueue::Node::getWeight() const {
   return m_weight;
}


void PQueue::Node::setWeight(Node::ValueType w) {
   m_weight = w;
}

PQueue::Node::operator unsigned() const {
   return getKey();
}

std::ostream& operator<<(std::ostream &out, const PQueue::Node &n) {
   out << n.m_key << " (" << n.m_weight << ')';
   return out;
}
