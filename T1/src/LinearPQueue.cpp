#include "LinearPQueue.h"
#include "Util.h"
#include <limits>

LinearPQueue::LinearPQueue() {
   // Vazio.
}


LinearPQueue::~LinearPQueue() {
   // Vazio.
}

PQueue::Node LinearPQueue::deletemin() {
   PQueue::Node choose{0, std::numeric_limits<PQueue::Node::ValueType>::max()};
   std::size_t index = 0;
   for (std::size_t i = 0; i < m_nodes.size(); ++i) {
      if (m_nodes[i] < choose) {
         choose = m_nodes[i];
         index = i;
      }
   }
   m_nodes.erase(m_nodes.begin() + index);
   return choose;
}

bool LinearPQueue::empty() const {
   return m_nodes.empty();
}

void LinearPQueue::insert(PQueue::Node::KeyType vertex, PQueue::Node::ValueType weight) {
   m_nodes.push_back({vertex, weight});
}

void LinearPQueue::decreasekey(PQueue::Node::KeyType vertex, PQueue::Node::ValueType newWeight) {
   for (PQueue::Node &i: m_nodes) {
      if (i == vertex) {
         i.setWeight(newWeight);
         break;
      }
   }
}

std::ostream& operator<<(std::ostream &out, const LinearPQueue &heap) {
   for (const PQueue::Node &i: heap.m_nodes) {
      out << i << std::endl;
   }
   return out;
}


