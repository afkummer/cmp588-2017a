#pragma once

#include "PQueue.h"
#include <vector>
#include <unordered_map>

class Heap {
public:
   /** Obedece a variável de ambiente `HEAP_ARITY` para definição
    * da aridade do heap.
    */
   Heap();
   ~Heap();

   bool empty() const;
   unsigned arity() const;
   std::size_t swapCount() const;

   PQueue::Node deletemin();
   void insert(PQueue::Node::KeyType vertex, PQueue::Node::ValueType weight);
   void decreasekey(PQueue::Node::KeyType vertex, PQueue::Node::ValueType newWeight);

   void reserve(std::size_t cap);
   const std::vector <PQueue::Node> &raw() const;

   friend std::ostream &operator<<(std::ostream &out, const Heap &h);

private:
   std::vector <PQueue::Node> m_vec;
   std::unordered_map <PQueue::Node::KeyType, PQueue::Node::KeyType> m_map; // key -> vec index
   std::size_t m_swapCount{0};
   unsigned m_arity;

   unsigned child(unsigned parent, unsigned th) const;
   unsigned parent(unsigned child) const;
   void siftUp(unsigned pos);
   void siftDown(unsigned pos);
};
