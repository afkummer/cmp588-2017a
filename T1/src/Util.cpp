#include "Util.h"
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <unistd.h>
#include <cassert>

unsigned long memusage() {
   std::ifstream fid("/proc/self/statm");
   unsigned long vm;
   fid >> vm;
   fid.close();
   return vm * static_cast<unsigned long>(getpagesize());
}

double walltime() {
   struct timeval time;
   gettimeofday(&time, NULL);
   return (double) time.tv_sec + (double) time.tv_usec * .000001;
}

