#include "Heap.h"
#include "Util.h"
#include <iostream>
#include <cstdlib>

Heap::Heap() {
   char *ch = std::getenv("HEAP_ARITY");
   if (ch == nullptr) {
      m_arity = 2;
   } else {
      m_arity = std::stoi(ch);
   }
}

Heap::~Heap() {
   // Vazio.
}

bool Heap::empty() const {
   return m_vec.empty();
}

void Heap::insert(PQueue::Node::KeyType vertex, PQueue::Node::ValueType weight) {
   m_vec.push_back({vertex, weight});
   m_map[vertex] = m_vec.size()-1;
   siftUp(m_vec.size()-1);
}

PQueue::Node Heap::deletemin() {
   PQueue::Node root = m_vec.front();
   m_vec.front() = m_vec.back();
   m_vec.pop_back();
   m_map[m_vec.front()] = 0;
   siftDown(0);
   return root;
}

void Heap::decreasekey(PQueue::Node::KeyType vertex, PQueue::Node::ValueType newWeight) {
   const std::size_t pos = m_map[vertex];
   PQueue::Node &n = m_vec[pos];
   n.setWeight(newWeight);
   siftUp(pos);
}

std::ostream& operator<<(std::ostream &out, const Heap &h) {
   out << "digraph Heap_" << h.m_arity << " {" << std::endl;
   bool done = false;
   for (unsigned i = 0; !done; ++i) {
      for (unsigned j = 0; j < h.m_arity; ++j) {
         const unsigned pos = h.child(i, j);
         if (pos >= h.m_vec.size()) {
            done = true;
            break;
         }
         out << '\t' << h.m_vec[i].getKey() << " -> " << h.m_vec[pos].getKey() << ';' << std::endl;
      }
   }
   out << "}";
   return out;
}

unsigned Heap::arity() const {
   return m_arity;
}

unsigned Heap::child(unsigned parent, unsigned th) const {
   return parent *m_arity + th + 1;
}

unsigned Heap::parent(unsigned child) const {
   return (child-1) / m_arity;
}

void Heap::siftUp(unsigned pos) {
   while (pos != 0) {
      const unsigned p = parent(pos);
      if (m_vec[p] > m_vec[pos]) {
         ++m_swapCount;
         std::swap(m_vec[p], m_vec[pos]);
         std::swap(m_map[m_vec[p]], m_map[m_vec[pos]]);
         pos = p;
      } else {
         break;
      }
   }
}

void Heap::siftDown(unsigned pos) {
   while (true) {
      unsigned next = child(pos, 0);
      for (unsigned i = 1; i < m_arity; ++i) {
         unsigned ch = child(pos, i);
         if (ch >= m_vec.size())
            break;
         if (m_vec[next] > m_vec[ch]) {
            next = ch;
         }
      }
      if (next < m_vec.size() && m_vec[next] < m_vec[pos]) {
         ++m_swapCount;
         std::swap(m_vec[next], m_vec[pos]);
         std::swap(m_map[m_vec[next]], m_map[m_vec[pos]]);
         pos = next;
      } else {
         break;
      }
   }
}

void Heap::reserve(std::size_t cap) {
   m_vec.reserve(cap);
   m_map.reserve(cap);
}

std::size_t Heap::swapCount() const {
   return m_swapCount;
}

const std::vector< PQueue::Node > &Heap::raw() const {
   return m_vec;
}
