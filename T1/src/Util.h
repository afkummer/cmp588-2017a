#pragma once

#include <iosfwd>

/**
 * @return Tempo em relógio de parede do processo.
 */
double walltime();

/**
 * Pergunta ao kernel o consumo de memória corrente da aplicação.
 * @return Memória utilizada pelo processo, em KiB.
 */
unsigned long memusage();


