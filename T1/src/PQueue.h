#pragma once

#include <ostream>

/**
 * @brief Interface básica para interação com uma fila de prioridade.
 * @author alberto
 */
class PQueue {
public:
   const static unsigned OP_INSERT = 0;
   const static unsigned OP_DECREASEKEY = 1;
   const static unsigned OP_DELETEMIN = 2;
   class Node {
   public:
      using KeyType = unsigned;
      using ValueType = unsigned;
      Node();
      Node(KeyType key, ValueType weight);
      inline bool operator<(const Node &other) const {
         return m_weight < other.m_weight;
      }
      inline bool operator>(const Node &other) const {
         return m_weight > other.m_weight;
      }
      operator std::size_t() const;
      operator unsigned() const;
      bool operator==(const KeyType key);
      KeyType getKey() const;
      ValueType getWeight() const;
      void setWeight(ValueType w);
      friend std::ostream& operator<<(std::ostream &out, const Node &n);
   private:
      KeyType m_key;
      ValueType m_weight;
   };
};
