#!/bin/bash

INST=(USA-road-d.NY.gr USA-road-d.BAY.gr USA-road-d.COL.gr USA-road-d.FLA.gr USA-road-d.NW.gr USA-road-d.NE.gr USA-road-d.CAL.gr USA-road-d.LKS.gr USA-road-d.E.gr USA-road-d.W.gr USA-road-d.CTR.gr USA-road-d.USA.gr)

for inst in ${INST[@]}
do

   gsize=`./graph-size < ../instances/$inst`

   origin=$RANDOM
   let "origin %= $gsize"

   dest=$RANDOM
   let "dest %= $gsize"

   echo "Testing algorithms with instance $inst, origin $origin and destination $dest"

   export QUEUE=linear
   echo "QUEUE linear"
   ./dijkstra-solver $origin $dest < ../instances/$inst

   for i in 2 4 8 16 32 64 128 256 512 1024
   do
      export QUEUE=heap ARITY=$i

      echo "Heap-"$i
      ./dijkstra-solver  $origin $dest < ../instances/$inst

   done

done

