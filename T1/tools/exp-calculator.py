#!/usr/bin/env python3
#! -*- coding: utf-8 -*-

from math import pow

def parent(i):
   return int((i-1)/2)

def left(i):
   return i * 2 + 1

def right(i):
   return i * 2 + 2

def siftUp(q, pos):
   swapcount = 0
   while pos != 0:
      if q[parent(pos)] > q[pos]:
         q[parent(pos)], q[pos] = q[pos], q[parent(pos)]
         swapcount += 1
         pos = parent(pos)
      else:
         break
   return swapcount

def insert(q, v):
   q.append(v)
   return siftUp(q, len(q)-1)

def wc_siftdown(h):
   count = 0
   for i in range(1, h+1):
      count += (i-1) * pow(2, i-1)
   return count

def wc_siftdown_compact(h):
   return (h-1) * pow(2, h+1) - h * pow(2, h) + 2

def wc_siftdown_ritt(h):
   return pow(2, h)*h

def savedot(q, name):
   with open(name, "wt") as fid:
      fid.write("digraph Heap {\n")
      for i in range(len(q)):
         lc = left(i)
         rc = right(i)
         if lc < len(q):
            fid.write("\t %d -> %d;\n" % (i, lc))
         else:
            break
         if rc < len(q):
            fid.write("\t %d -> %d;\n" % (i, rc))
         else:
            break
      fid.write("}\n")

def verify_heapness(q):
   for i in range(len(q)):
      lc = left(i)
      rc = right(i)
      if lc < len(q):
         if q[i] > q[lc]:
            print("l Algo errado na posição %d: %d é pai de %d!\n" % (i, q[i], q[lc]))
      else:
         break
      if rc < len(q):
         if q[i] > q[rc]:
            print("r Algo errado na posição %d: %d é pai de %d!\n" % (i, q[i], q[rc]))
      else:
         break

def render_dot(q, h, i):
   from os import system
   savedot(q, "tmp.dot")
   fname = "heap-" + str(h) + "-" + str(i) + ".png"
   system("dot < tmp.dot > %s -T png" % fname)


if __name__ == "__main__":
   print("h  sw_loop sp_compact sp_ritt sp_real")
   for i in range(4, 5):
      q = []
      scount = 0
      for j in range(int(pow(2, i))-1, 0, -1):
         render_dot(q, i, j)
         scount += insert(q, j)
      print("%2d %6d %9d %7d %7d" % (i, wc_siftdown(i), wc_siftdown_compact(i), wc_siftdown_ritt(i), scount))
      #savedot(q, "output.dot")
      verify_heapness(q)




