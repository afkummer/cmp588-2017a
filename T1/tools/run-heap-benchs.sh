#!/bin/bash

echo "Running insert benchmark..."
./heap-insert-bench | tee "insert-bench.txt"

echo "Running decreasekey benchmark..."
./heap-insert-bench | tee "decreasekey-bench.txt"

echo "Running deletekey benchmark..."
./heap-insert-bench | tee "deletekey-bench.txt"

