\documentclass{iiufrgs}
\usepackage[utf8]{inputenc}   % pacote para acentuação
\usepackage[T1]{fontenc}
\usepackage{graphicx}           % pacote para importar figuras
\usepackage{times}              % pacote para usar fonte Adobe Times
\usepackage{framed}             % para exemplos e TODOs
\usepackage{biblatex}           % para referências bibliográficas
\usepackage{xcolor}             % cores
\usepackage{hyperref}           % referências
\usepackage{amsmath}
\usepackage{inconsolata}
\usepackage{booktabs}
\usepackage{bm}
\usepackage{tabularx}

\graphicspath{{./img/}}

\colorlet{shadecolor}{orange!15}
\overfullrule=10pt

\title{Trabalho 1\\Heaps binários e Algoritmo de Dijkstra}
\author{}{Alberto Francisco Kummer Neto}

\addbibresource{relatorio.bib}

\begin{document}
\maketitle

\setcounter{chapter}{1}

\section{Tarefa}
A tarefa apresentada neste relatório é composta de três partes distintas. A primeira parte trata da verificação do tempo de execução das operações \texttt{insert}, \texttt{decreasekey} e \texttt{deletemin} sobre um \textit{heap} binário, com ênfase no número de \textit{swaps} efetuados. É conduzido um experimento para demonstrar que o tempo demandado para execução obedece o limite assintótico teórico das operações.

Analogamente, a segunda parte desta tarefa apresenta uma comparação entre complexidade real e complexidade teórica demandada pelo algoritmo de busca em grafo de Dijkstra, variando-se o número de arestas e vértices do grafo de entrada. \textcolor{blue}{Ainda tem que descrever quais levemente os experimento conduzido.}

Por fim, a terceira parte desta tarefa apresenta os resultados computacionais para instâncias reais do problema de busca de menor caminho em grafo com Dijkstra usando como caso de teste os grafos \textit{NY} e \textit{USA} do \textit{dataset} DIMACS \cite{demetrescu2009}. Também são apresentados os números de operações \texttt{insert}, \texttt{decreasekey} e \texttt{deletemin} feitas sobre a estrutura de dados \textit{heap}.

\section{Solução}

Considere um \textit{min-heap} binário de altura $h$. Tal heap pode ser construído usando-se $2^{h-1}-1$ elementos, com a garantia de que todos os seus níveis estejam completos. De maneira análoga, cada nível $l = 1, \dots, h$ possuirá $2^{l-1}$ elementos, conforme ilustração da Figura \ref{heap-4}.

\begin{figure}[!htb]
   \centering
   \includegraphics[keepaspectratio,width=0.8\textwidth]{img/heap-4}
   \label{heap-4}
   \caption{Número de nodos para cada nível de um \textit{heap} binário de altura $h=4$.}
\end{figure}

O pior caso do número de operações \textit{swap} do \textit{min-heap} pode ser artificialmente alcançado fazendo-se $2^h$ operações de inserção em um \textit{heap} vazio, inserindo-se os elementos em ordem descrescente de peso ($2^h-1, 2^h-2, 2^h-3, \dots, 1$). Seguindo essa ordem de inserção, cada nível $l, l = 1, \dots, h$ do \textit{heap} contabilizará $(l-1) \cdot 2^{l-1}$ operações de \textit{swap}. Logo, o número total esperado de operações de \textit{swap} em um \textit{min-heap} de altura $h$ é computado por:

\begin{align}
   \operatorname{swapcount}(h) &= \sum_{i=1}^{h} (i-1) \cdot 2^{i-1}\nonumber\\
   &= (h-1) \cdot 2^{h+1} - h \cdot 2^h + 2\nonumber
\end{align}

A implementação da estrutura de dados \textit{heap} foi feita na linguagem de programação C++. A implementação em questão fez uso da representação da árvore em um \textit{array}, o que trouxe complicações para uma implementação eficiente da operação \texttt{decreasekey}. Para manter a complexidade da operação dentro do limite teórico $O(\log n)$, usou-se uma estrutra de dados auxiliar para resolver o mapeamento de um elemento do \textit{heap} e sua posição dentro do \textit{array}.

\section{Ambiente de teste}

Os testes computacionais foram conduzidos em uma microcomputador equipado com
um processador Intel i5 4440S, operando a $2.80$ GHz, em conjunto a $8$ GiB
de memória RAM.

\begin{table}[!htb]
\centering
\begin{tabular}{@{}crr@{}}
\toprule
Altura do \textit{heap} & Número de nodos & Consumo de memória (em MiB) \\ \midrule
1                         & 2               & 0,000                       \\
2                         & 4               & 0,000                       \\
3                         & 8               & 0,000                       \\
4                         & 16              & 0,000                       \\
5                         & 32              & 0,000                       \\
6                         & 64              & 0,000                       \\
7                         & 128             & 0,001                       \\
8                         & 256             & 0,002                       \\
9                         & 512             & 0,004                       \\
10                        & 1024            & 0,008                       \\
11                        & 2048            & 0,016                       \\
12                        & 4096            & 0,031                       \\
13                        & 8192            & 0,062                       \\
14                        & 16384           & 0,125                       \\
15                        & 32768           & 0,250                       \\
16                        & 65536           & 0,500                       \\
17                        & 131072          & 1,000                       \\
18                        & 262144          & 2,000                       \\
19                        & 524288          & 4,000                       \\
20                        & 1048576         & 8,000                       \\
21                        & 2097152         & 16,000                      \\
22                        & 4194304         & 32,000                      \\
23                        & 8388608         & 64,000                      \\
24                        & 16777216        & 128,000                     \\
25                        & 33554432        & 256,000                     \\
26                        & 67108864        & 512,000                     \\
27                        & 134217728       & 1024,000                    \\
28                        & 268435456       & 2048,000                    \\
29                        & 536870912       & 4096,000                    \\
30                        & 1073741824      & 8192,000                    \\
31                        & 2147483648      & 16384,000                   \\
32                        & 4294967296      & 32768,000                   \\
33                        & 8589934592      & 65536,000                   \\
34                        & 17179869184     & 131072,000                  \\ \bottomrule
\end{tabular}
\caption{Consumo de memória do \textit{heap} binário com nodos de 8 bytes.}
\label{table:memory-consumption}
\end{table}

Sabendo-se que o custo de inserção de $n$ elementos em um \textit{heap} é $O(\log n)$, é esperado que, para uma série de valores de $n$, o tempo das operações de \textit{insert} sejam:

\begin{align*}
   T(1) &= c_1 \cdot \log (1)\nonumber\\
   T(2) &= c_2 \cdot \log (2)\nonumber\\
   &\dots\nonumber\\
   T(2^{i-2}) &= c_{i-2} \cdot \log (2^{i-2})\nonumber\\
   T(2^{i-1}) &= c_{i-1} \cdot \log (2^{i-1})\nonumber
\end{align*}

O termo que domina a função do tempo da operação de inserção em um \textit{heap} é $\log n$, e os valores $c$ tendem a se manter os mesmos, independente do valor da entrada. Assim, é possível afirmar que $T(n) = c \cdot \log(n)$ e que $c_1 \approx c_2 \approx \dots \approx c_{i-2} \approx c_{i-1}$, como pode ser verificado pelos resultados computacionais da Tabela \ref{table:ajuste-c-insert}.

\input{img/plot-insert-dec}

\begin{table}[!htb]
\centering
   \begin{tabular}{@{}cccccc@{}}
      \toprule
      $h$ & $2^h-1$   & Tempo (segundos) & swapcount (esp) & swapcount  & $c$          \\ \midrule
      2   & 3         & 9.720E-06        & 2               & 2          &       \\
      3   & 7         & 1.240E-05        & 10              & 10         & 1.275 \\
      4   & 15        & 1.630E-05        & 34              & 34         & 1.314 \\
      5   & 31        & 2.124E-05        & 98              & 98         & 1.303 \\
      6   & 63        & 4.658E-05        & 258             & 258        & 2.192 \\
      7   & 127       & 7.157E-05        & 642             & 642        & 1.536 \\
      8   & 255       & 1.176E-04        & 1538            & 1538       & 1.643 \\
      9   & 511       & 1.943E-04        & 3586            & 3586       & 1.652 \\
      10  & 1023      & 3.685E-04        & 8194            & 8194       & 1.896 \\
      11  & 2047      & 7.413E-04        & 18434           & 18434      & 2.011 \\
      12  & 4095      & 2.466E-03        & 40962           & 40962      & 3.326 \\
      13  & 8191      & 3.681E-03        & 90114           & 90114      & 1.493 \\
      14  & 16383     & 6.498E-03        & 196610          & 196610     & 1.765 \\
      15  & 32767     & 1.286E-02        & 425986          & 425986     & 1.979 \\
      16  & 65535     & 2.801E-02        & 917506          & 917506     & 2.177 \\
      17  & 131071    & 4.770E-02        & 1966082         & 1966082    & 1.702 \\
      18  & 262143    & 1.036E-01        & 4194306         & 4194306    & 2.171 \\
      19  & 524287    & 1.765E-01        & 8912898         & 8912898    & 1.704 \\
      20  & 1048575   & 3.525E-01        & 18874370        & 18874370   & 1.996 \\
      21  & 2097151   & 7.197E-01        & 39845890        & 39845890   & 2.042 \\
      22  & 4194303   & 1.473E+00        & 83886082        & 83886082   & 2.047 \\
      23  & 8388607   & 3.018E+00        & 176160770       & 176160770  & 2.048 \\
      24  & 16777215  & 6.157E+00        & 369098754       & 369098754  & 2.039 \\
      25  & 33554431  & 1.257E+01        & 771751938       & 771751938  & 2.041 \\
      26  & 67108863  & 2.567E+01        & 1610612738      & 1610612738 & 2.042 \\
      27  & 134217727 & 5.237E+01        & 3355443202      & 3355443202 & 2.040 \\
      28  & 268435455 & 1.065E+02        & 6979321858      & 2684354562 & 2.034 \\
      29  & 536870911 & 2.174E+02        & 14495514626     & 1610612738 & 2.041 \\ \bottomrule
   \end{tabular}
   \caption{Dar um caption pra isso.}
   \label{table:ajuste-c-insert}
\end{table}




% Testamos com dados gerados randomicamente de tamanho $n=2,4,8,\ldots,2^{12}$. Cada teste foi repetido $20$ vezes.

\section{Resultados}

\section{Conclusão}

\printbibliography

\end{document}
