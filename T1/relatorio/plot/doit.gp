#set terminal epslatex
#set output "plot-insert-dec.tex"

set terminal pdf
set output "plot-insert-dec.pdf"

set ylabel "$\\log 2^i$ nodos"
set xlabel "Comportamento assintótico"

set autoscale

plot "dados.csv" using 7:1 title 'Curva encontrada' with linespoints lw 2 ps 1.5 lc rgb '#0000ff'

